#include <iostream>

using namespace std;


typedef struct nodo{
    int dato;
    nodo *sgte;
} *Tnodo;


void insertar(Tnodo &lista, int data){
    Tnodo nuevo =  NULL;
    nuevo =  new nodo;
    nuevo->dato = data;
    nuevo->sgte = NULL;

    Tnodo p = lista;

    if(lista->dato == 0){
        lista = nuevo;
    }

    while(p->sgte!=NULL){
        p = p->sgte;
    }

    p->sgte = nuevo;

}

int size(Tnodo &lista){
    Tnodo p = lista;

    int n = 0;

    while(p!=NULL){
        p = p->sgte;
        n++;
    }

    return n;

}

Tnodo nodo_por_pos(Tnodo &lista, int pos ){
    Tnodo p = lista;
    int n = 1;

    while(pos != n){

        p = p->sgte;
        n++;
    }
    return p;
}

Tnodo nodo_por_ind(Tnodo &lista, int pos ){
    Tnodo p = lista;
    int n = 0;

    while(pos != n){

        p = p->sgte;
        n++;
    }
    return p;
}

void listar(Tnodo &lista){
    string enter;
    Tnodo p = lista;

    int n = 1;

    cout<<"\t\t\t\tElementos"<<endl;
    cout<<"\t\t\t\t========="<<endl;

    while(p!=NULL){
        cout<<"\t\t\t\t("<<n<<") "<<p->dato<<endl;
        p = p->sgte;
        n++;
    }

    cout<<"\n\t\t\t\t"<<"num. de elementos: "<<(n - 1)<<endl;
    cin.ignore(1024, '\n');
    cout << "\t\t\t\tEnter para regresar al menu ..."<<endl;
    cin.get();
}

void intercambiar(Tnodo &nodo1, Tnodo &nodo2){
    int aux = nodo1->dato;
    nodo1->dato = nodo2->dato;
    nodo2->dato = aux;
}


Tnodo seleccion(Tnodo &lista){

    Tnodo copia = NULL;
    copia =  new nodo;
    copia->dato = NULL;
    copia->sgte = NULL;

    Tnodo p = lista;

    while(p!=NULL){
        insertar(copia, p->dato);
        p = p->sgte;
    }


    for(int i=1; i<=(size(copia)-1); i++){
        int minimo = i;
        for( int j = i+1; j<=(size(copia)); j++){
            Tnodo nodo_j = nodo_por_pos(copia,j);
            Tnodo nodo_min = nodo_por_pos(copia,minimo);

            if(nodo_j->dato < nodo_min->dato){
                minimo = j;
            }
        }
        Tnodo nodo_i = nodo_por_pos(copia,i);
        Tnodo nodo_min = nodo_por_pos(copia,minimo);
        intercambiar(nodo_i, nodo_min);
    }


    return copia;
}

Tnodo insercion(Tnodo &lista){

    Tnodo copia = NULL;
    copia =  new nodo;
    copia->dato = NULL;
    copia->sgte = NULL;

    Tnodo p = lista;

    while(p!=NULL){
        insertar(copia, p->dato);
        p = p->sgte;
    }

    int i, j ,tmp;

    for (i = 1; i < size(copia); i++) {
         j = i;
         while (j > 0 && nodo_por_ind(copia,j - 1)->dato > nodo_por_ind(copia,j)->dato ) {
             tmp = nodo_por_ind(copia,j)->dato;
             nodo_por_ind(copia,j)->dato = nodo_por_ind(copia,j -1)->dato;
             nodo_por_ind(copia,j -1)->dato = tmp;
             j--;
         }
    }

    return copia;
}

Tnodo burbuja(Tnodo &lista){

    Tnodo copia = NULL;
    copia =  new nodo;
    copia->dato = NULL;
    copia->sgte = NULL;

    Tnodo p = lista;

    while(p!=NULL){
        insertar(copia, p->dato);
        p = p->sgte;
    }

    int c, d, swap;

    for (c = 0 ; c < ( size(copia) - 1 ); c++)
      {
        for (d = 0 ; d < size(copia) - c - 1; d++)
        {
          if ( nodo_por_ind(copia,d)->dato > nodo_por_ind(copia,d+1)->dato )
          {
            swap       = nodo_por_ind(copia,d)->dato;
            nodo_por_ind(copia,d)->dato = nodo_por_ind(copia,d+1)->dato;
            nodo_por_ind(copia,d+1)->dato = swap;
          }
        }
      }

    return copia;
}

Tnodo intercambio(Tnodo &lista){
    Tnodo copia = NULL;
    copia =  new nodo;
    copia->dato = NULL;
    copia->sgte = NULL;

    Tnodo p = lista;

    while(p!=NULL){
        insertar(copia, p->dato);
        p = p->sgte;
    }

    int ni = 0;
    int i, temp, j;
    for(i = 1; i < size(copia); i++){
        temp = nodo_por_ind(copia,i)->dato;
        ni++;
        j = i - 1;
        while( (j>=0) && (nodo_por_ind(copia,j)->dato > temp) ){
             nodo_por_ind(copia,j+1)->dato =  nodo_por_ind(copia,j)->dato;
             j--;
        }
        nodo_por_ind(copia,j+1)->dato = temp;
    }
    return copia;

}



void men_insertar(Tnodo &lista){
    int num;
    cout<<"\t\t\t\tIngrese numero a insertar: ";
    cin>>num;
    insertar(lista, num);
}

void menu(Tnodo &lista){
    int opc;

    do{
        cout << "\033[2J\033[1;1H";
        cout << "\t\t\tÉÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ»" << endl;
        cout << "\t\t\tº                MENU                   º" << endl;
        cout << "\t\t\tÌÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ¹" << endl;
        cout << "\t\t\tº  1> Agregar elemento                  º" << endl;
        cout << "\t\t\tº  2> Mostrar lista                     º" << endl;
        cout << "\t\t\tº  3> Ordenamiento por met. seleccion   º" << endl;
        cout << "\t\t\tº  4> Ordenamiento por met.insercion    º" << endl;
        cout << "\t\t\tº  5> Ordenamiento por met.intercambio  º" << endl;
        cout << "\t\t\tº  6> Ordenamiento por met.burbuja      º" << endl;
        cout << "\t\t\tÈÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍŒ" << endl;
        cout << "\t\t\t\tIngrese una opcion: ";

        cin>>opc;

        switch(opc){
            case 1: men_insertar(lista); break;
            case 2: listar(lista); break;
            case 3: {

                Tnodo ord = seleccion(lista);

                cout<<"\n\t\t\t\tListado ordenado usando seleccion"<<endl;
                cout<<"\t\t\t\t======================================="<<endl;
                listar(ord);

                break;
            }
            case 4: {

                Tnodo ord = insercion(lista);

                cout<<"\n\t\t\t\tListado ordenado usando Insercion"<<endl;
                cout<<"\t\t\t\t======================================="<<endl;
                listar(ord);

                break;
            }
            case 5: {

                Tnodo ord = intercambio(lista);

                cout<<"\n\t\t\t\tListado ordenado usando Insercion"<<endl;
                cout<<"\t\t\t\t======================================="<<endl;
                listar(ord);

                break;
            }
            case 6: {

                Tnodo ord = burbuja(lista);

                cout<<"\n\t\t\t\tListado ordenado usando burbuja"<<endl;
                cout<<"\t\t\t\t======================================="<<endl;
                listar(ord);

                break;
            }
       }

    }while(opc > 0);
}


int main(){

    Tnodo lista = NULL;
    lista =  new nodo;
    lista->dato = NULL;
    lista->sgte = NULL;


    // Data de prueba

    /*insertar(lista, 9);
    insertar(lista, 8);
    insertar(lista, 7);
    insertar(lista, 6);
    insertar(lista, 5);
    insertar(lista, 4);
    insertar(lista, 2);
    insertar(lista, 3);
    */
    menu(lista);

    return 0;
}
